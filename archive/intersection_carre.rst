Géométrie: intersection de carrés
---------------------------------

Ecrire une fonction qui indique si deux carrés définis par leur coin inférieur 
gauche et la longueur de leur coté s'intersectent

.. easypython:: ./intersection_carre/
   :language: Jacadi
   :titre: jour nuit
   :extra_yaml:
     fichier_ens: intersection_carre.py
