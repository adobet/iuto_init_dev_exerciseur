Géométrie: point dans un carré
-------------------------------

Ecrire une fonction qui indique si un point se situe à l'intérieur d'un
carré définit par son coin inférieur gauche et la longueur de son coté.

.. easypython:: ./point_dans_carre/
   :language: Jacadi
   :titre: point dans carre
   :extra_yaml:
     fichier_ens: point_dans_carre.py
