from point_dans_carre import est_dans


entrees_visibles = [(2,3,2,3,4.5,2),
                    (2,3,2,5.5,3.5,4),
                    (2,3,2,0,2,5)
]
entrees_invisibles = [(0,0,4,-8,-9,2),
                      (0,0,4,3,3,2)
]

@solution
def intersection(x1,y1,cote1,x2,y2,cote2):
    """ indique si deux carrés s'intersectent

    Args:
        x1 (float): abscisse du coin inférieur droit du carré 1
        y1 (float): ordonnée du coin inférieur droit du carré 1
        cote1 (float): longueur du coté du carré 1
        x2 (float): abscisse du coin inférieur droit du carré 2
        y2 (float): ordonnée du coin inférieur droit du carré 2
        cote2 (float): longueur du coté du carré 2
        
    Returns:
        bool: True si les carrés s'intersectent False sinon
    """    
    if est_dans(x1,y1,cote1,x2,y2):
        res=True
    elif est_dans(x1,y1,cote1,x2+cote2,y2):
        res=True
    elif est_dans(x1,y1,cote1,x2,y2+cote2):
        res=True
    elif est_dans(x1,y1,cote1,x2+cote2,y2+cote2):
        res=True
    elif est_dans(x2,y2,cote2,x1,y1):
        res=True
    elif est_dans(x2,y2,cote2,x1+cote1,y1):
        res=True
    elif est_dans(x2,y2,cote2,x1,y1+cote1):
        res=True
    elif est_dans(x2,y2,cote2,x1+cote1,y1+cote1):
        res=True
    else:
        res=False
    return res
