def est_dans(x_c,y_c,cote,x_p,y_p):
    """ indique si un point est à l'intérieur d'un carré

    Args:
        x_c (float): abscisse du coin inférieur droit du carré
        y_c (float): ordonnée du coin inférieur droit du carré
        cote (float): longueur du coté du carré
        x_p (float): abscisse du point
        y_p (float): ordonnée du point

    Returns:
        bool: True si le point est à l'intérieur du carré False sinon
    """    
    if x_p<x_c:
        res=False
    elif x_p>x_c+cote:
        res=False
    elif y_p<y_c:
        res=False
    elif y_p>y_c+cote:
        res=False
    else:
        res=True
    return res
