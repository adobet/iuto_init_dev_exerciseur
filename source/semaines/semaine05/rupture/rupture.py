entrees_visibles = [([1,5,-3,40,32],10),
                    ([1,5,-3,5,3],10,)
]
entrees_invisibles = [
        ([14,23,10,8,7],10),
        ([-4,1,-4,3,8],10),
        ([],10)
]

@solution
def rupture(liste,seuil):
    """
    recheche l'indice de la première rupture d'une liste
    paramètres: liste une liste de nombres
                seuil un nombre indiquant le seuil de rupture
    résultat: l'indice de la rupture ou -1 s'il n'y en a pas
    """
    # je choisis une boucle while pour m'arrêter dès que possible
    i=1
    trouve=False
    while i<len(liste) and not trouve:
    #invariant: trouve <=> liste[i-1] est une rupture
        if abs(liste[i-1]-liste[i])>seuil:
            trouve=True
        else:
            i+=1
    if trouve:
        res=i
    else:
        res=-1
    return res
