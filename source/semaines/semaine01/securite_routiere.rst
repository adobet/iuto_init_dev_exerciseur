Sécurité routière
-----------------

Consulter le site https://www.controleradar.org/contraventions.html pour connaitre la
réglementation en terme de contravention en cas d'excès de vitesse.

On souhaiterait avoir une fonction qui nous donne les sanctions
encourues en fonction de l'excès de vitesse que l'on a commis.


.. easypython:: ./exosecu-routiere/
   :language: Jacadi
   :titre: secu routiere
   :extra_yaml:
     fichier_ens: exosecu-routiere.py
     auto_hypothesis: True


