Qualification aux jeux olympiques
---------------------------------

La fédération d'athlétisme vient de publier les critères de qualification à l'épreuve du 100 m des jeux olympiques. Ces critères sont les suivants:
- Pour les hommes
  
  - soit il faut avoir un record personnel au 100m inférieur à 12 secondes et avoir gagné au moins 3 courses dans l'année
    
  - soit être champion du monde de la discipline
    
- Pour les femmes
  
  - soit il faut avoir un record personnel au 100m inférieur à 15 secondes et avoir gagné au moins 3 courses dans l'année
    
  - soit être championne du monde de la discipline


.. easypython:: ./exoqualif-jo/
   :language: Jacadi
   :titre: qualif-jo
   :extra_yaml:
     fichier_ens: exoqualif-jo.py


