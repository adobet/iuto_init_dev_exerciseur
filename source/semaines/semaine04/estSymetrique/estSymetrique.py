entrees_visibles = [[1,2,3,2,1],
                    [1,2,3,1],
                    [],
]
entrees_invisibles = [
        [1],
        [1,2,2,1],
]

@solution
def estSymetrique(liste):
  """
  """ 
  i=0
  fin=len(liste)-1
  symetrique=True
  while symetrique and i<len(liste)/2:
    symetrique = (liste[i]==liste[fin-i])
    i+=1
  return symetrique
