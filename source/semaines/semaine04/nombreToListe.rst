Liste des chiffres d'un nombre
==============================

On voudrait stocker les chiffres d'un nombre entier (positif) dans une liste
en mettant les unités dans le premier élément de cette liste.

\'Ecrire une fonction qui résoud ce problème.

.. easypython:: ./nombreToListe/
   :language: Jacadi
   :titre: nombreToListe
   :extra_yaml:
     fichier_ens: nombreToListe.py
