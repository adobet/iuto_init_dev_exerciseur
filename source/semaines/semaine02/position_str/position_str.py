entrees_visibles = [('a','programmation'),
                    ('z','programmation'), 
                    ('a',''),                
]
entrees_invisibles = [
                    ('p','programmation'), 
                    ('n','programmation'),                  
                    ('m','programmation'),
]

@solution
def position(lettre, mot) :
  for i in range(len(mot)):
    if lettre==mot[i]:
      return i
  return -1
