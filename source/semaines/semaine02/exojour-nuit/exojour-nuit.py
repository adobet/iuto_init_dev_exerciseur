entrees_visibles = [10,
                    2,
                    16,
                    -5
]
entrees_invisibles = [
        6,
        20,
        12
]

@solution
def jour_nuit(heure) :
    """
    cette fonction permet de connaitre à partir d'une heure donnée 
    (comprise entre 0 et 24) s'il fait jour ou nuit et dans quelle partie 
    de la journée on se trouve. 
    """
    if heure < 0 or heure >= 24 :
        moment = "heure incorrecte"
    else :
        # l'heure est correcte
        if heure >= 7 :
            if heure < 19 :
                moment = "il fait jour "
            else :
                #heure >= 7 et heure >= 19
                moment = "il fait nuit "
        else :
            #heure < 7
            moment = "il fait nuit "
        # on traite les parties de journée
        if heure >= 6 :
            if heure < 12 :
                moment = moment + "et c'est le matin"
            else :
                #heure >= 12
                if heure < 18 :
                    moment = moment + "et c'est l'après midi"
                else :
                    #heure >= 18
                    if heure < 21 :
                        moment = moment + "et c'est la soirée"
                    else :
                        #heure >= 21 
                        moment = moment + "et c'est la nuit"
        else :
            # heure < 6
            moment = moment + "et c'est la nuit"
    return moment
