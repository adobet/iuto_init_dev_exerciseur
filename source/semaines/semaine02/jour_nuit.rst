Jour-nuit
---------

Il fait jour de 7h à 19h, il fait nuit le reste du temps. C’est le
matin de 6h à 12h, l’après midi de 12h à 18h, la soirée de 18h à 21h,
la nuit le reste du temps. Écrire script Python qui permet, à partir
d'un entier compris entre 0 et 24, d'indiquer dans quelle partie de
la journée on est et s'il fait jour ou nuit. Pour éviter toute
ambiguïté, les intervalles de temps seront traités sous la forme:
heuredébut <= t < heurefin.

.. easypython:: ./exojour-nuit/
   :language: Jacadi
   :titre: Jour nuit
   :tags:
     - cond
   :extra_yaml:
     fichier_ens: exojour-nuit.py
