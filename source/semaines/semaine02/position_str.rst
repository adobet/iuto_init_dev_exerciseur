Position
========

Ecrire une fonction renvoie la  position  de  la  première  apparition  de  la  lettre  (la  première  lettre  du  mot  ayant  la
position 0). Si la lettre n’apparait pas dans le mot on souhaite obtenir le résultat -1


.. easypython:: ./position_str/
   :language: Jacadi
   :titre: position str
   :extra_yaml:
     fichier_ens: position_str.py

