:orphan:

.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Semaine 1
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine01/*
   semaine01/exercices_bonus/*


Semaine 2
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine02/*


Semaine 3
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine03/*

Semaine 4
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine04/*

Semaine 5
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine05/*

Semaine 6
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine06/*

Semaine 7
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine07/*
