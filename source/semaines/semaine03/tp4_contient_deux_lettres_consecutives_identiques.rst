

Contient deux lettres consecutives identiques
-----------------------------------------------

Écrire une fonction qui permet de savoir si
un mot contient au moins deux lettres consécutives identiques. Par
exemple *coucou* ne répond pas au critère alors que *creee* si.

.. easypython:: ./contient_deux_lettres_consecutives_identiques/
   :language: Jacadi
   :titre: contient_deux_lettres_consecutives_identiques
   :extra_yaml:
     fichier_ens: contient_deux_lettres_consecutives_identiques.py
