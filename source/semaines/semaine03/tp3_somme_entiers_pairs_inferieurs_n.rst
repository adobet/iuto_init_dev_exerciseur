

Somme des entiers pairs inferieurs à n
-----------------------------------------

Écrire une fonction qui fait la somme des
entiers paires parmi les n premiers entiers. Par exemple si n vaut 4
le résultat doit être 6 qui est le résultat de 2+4.

.. easypython:: ./somme_entiers_pairs_inferieurs_n/
   :language: Jacadi
   :titre: somme_entiers_pairs_inferieurs_n
   :extra_yaml:
     fichier_ens: somme_entiers_pairs_inferieurs_n.py
