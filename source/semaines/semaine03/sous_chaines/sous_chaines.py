entrees_visibles = [('bonjour', 2, 3,), 
                    ('informatique', 4, 3,), 
]

entrees_invisibles = [('bonjour', 1, 4,), 
                     ('informatique', 3, 19,), 
                     ('lundi', 45, 19,),
                     ('informatique', 2, 7,),
                     ('', 3, 19,),   
]


@solution
def extraire(ch, debut, longueur) : 
    """
    cette fonction permet d'extraire de ch une sous chaine débutant à l'indice 
    debut et ed longueur longueur
    si début est trop grand, le résultat est la chaine vide 
    si longueur est trop grand, on retourne la fin de la chaine. 
    """
    res = ''
    for position in range(debut, min(debut + longueur, len(ch))) : 
        res += ch[position]
    return res
