.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.




Exercices élémentaires
=======================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   elementaire/*

Travailler les conditionnelles
=================================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   conditionnelles/*

Fonctions de cumul
====================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   cumuls/*

Fonctions de vérification
==============================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   verifications/*

Des extremums
==================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   extremums/*


Pour travailler le while
==============================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   while/*


Avec des dictionnaires
===============================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   dico_simple/*


Structures de données imbriquées
==================================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   sdd_imbriquee/*


Utiliser la fonction sorted
=============================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   .. sorted/*


Autres exercices
==================

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   autres/*
