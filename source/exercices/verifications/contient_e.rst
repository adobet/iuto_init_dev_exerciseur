La chaine contient-elle un 'e' ?
---------------------------------

Écrire une fonction ```contient_e()``` qui vérifie qu'une chaine de caractères contient la lettre 'e'.


.. easypython:: ./contient_e/
   :language: Jacadi
   :titre: contient_e
   :extra_yaml:
     fichier_ens: contient_e.py

