:orphan:

Décodage d'une lettre
---------------------

Écrire une fonction *decodeLettre* qui retourne la lettre
  correspondant à un code en fonction d'une clé donnée.


.. easypython:: ./decodeLettre/
   :language: Jacadi
   :titre: decodeLettre
   :extra_yaml:
     fichier_ens: decodeLettre.py
