:orphan:

Codage d'une lettre
-------------------

Écrire une fonction *codeLettre* qui permet de calculer le
code d'une lettre dans une clé donnée (voir explications plus haut).


.. easypython:: ./codeLettre/
   :language: Jacadi
   :titre: codeLettre
   :extra_yaml:
     fichier_ens: codeLettre.py
