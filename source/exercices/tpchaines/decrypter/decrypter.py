entrees_visibles = [('0201901011019022026','nbvcxwmlkjhgfdsqpoiuy treza'),
                    ('0260160170270230402702302101504017027030180401603021015015022040170401403028016090130240301604010304029030150150170203','lge wnfkyhjpdsrazquiotxbvcm'),
]
entrees_invisibles = [
        ('010203040506070809011012013014015016017018019021022023024025026027','nbvcxwmlkjhgfdsqpoiuy treza'),
        ('010203040506070809011012013014015016017018019021022023024025026027','poirezuy tnwmlkjhgfbvcxdsqa')
]

@solution
def decrypter(texteCrypte, cle):
    res=''
    codeStr=''
    for lettre in texteCrypte:
        if lettre=='0':
            if codeStr!='':
                code=int(codeStr)
                res+=decodeLettre(code,cle)
                codeStr=''
        else:
            codeStr+=lettre
    code=int(codeStr)
    res+=decodeLettre(code,cle)
    return res

def decodeLettre(code,cle):
    if 1>code:
        res='?'
    else:
        code-=1+code//10
    if (code>=len(cle)):
        res='?'
    else:
        res=cle[code]
    return res
