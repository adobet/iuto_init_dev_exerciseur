:orphan:

Décryptage d'un texte
---------------------

En utilisant la fonction *decodeLettre*, écrire une fonction *decrypter* qui
permet de decrypter un texte suivant une clé donnée.
**ATTENTION** n'oubliez pas de recopier le code de la fonction *decodeLettre*


.. easypython:: ./decrypter/
   :language: Jacadi
   :titre: decrypter
   :extra_yaml:
     fichier_ens: decrypter.py
