afrique = [ ('Silki', 4420), ('Kilimandjaro', 5892), ('Mont Cameroun', 4040)]
asie = [('Trivor', 7577), ('Annapurna I', 8091), ('Everest', 8848), ('K2', 8611)]

a=[('a', 1), ('b', 3), ('c', 5), ('d', 2), ('e', 12), ('f', -18)]
r=[('a',0), ('b',2), ('c',32), ('d',8), ('e',16), ('f',4)]


entrees_visibles = [ afrique, asie ]
entrees_invisibles = [a, r]


@solution
def moyenne(liste):
    s = 0
    for _,altitude in liste:
        s+= altitude
    return s/len(liste)

#  for e in entrees_visibles+entrees_invisibles:
    #  print(moyenne(e))


