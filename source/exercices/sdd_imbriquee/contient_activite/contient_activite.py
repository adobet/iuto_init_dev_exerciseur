trois_amis = {
    'Camille' : {'Vélo', 'Kayak', 'Boxe'},
    'Dominique' : {'Vélo'},
    'Claude' : {'Lecture', 'Tricot', 'Boxe'}
    }

deux_amis = {
    'Camille' : {'Vélo', 'Kayak', 'Boxe'},
    'Claude' : {'Lecture', 'Tricot'}
    }
    
autre1 = {
    'a' : {'1', '2', '3'},
    'b' : {'4', '8'},
    'c' : {'2', '3', '4'}
    }

autre2 = {
    'a' : {'1', '2', '3'},
    'b' : {'4'},
    'c' : {'2', '3', '4'}
    }
    
    
entrees_visibles = [ deux_amis, trois_amis ]
entrees_invisibles = [ autre1, autre2 ]


@solution
def amis_difficiles(amis):
    for e in amis.values():
        if len(e)<2:
            return True
    return False

#  for p in entrees_visibles+entrees_invisibles:
    #  print(amis_difficiles(p))


