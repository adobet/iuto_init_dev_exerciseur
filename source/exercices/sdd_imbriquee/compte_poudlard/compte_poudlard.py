adrian = { "nom": "Adrian", "courage": 9,"malice" : 10, "maison": "Serpentar"}
hermione = { "nom": "Hermione", "courage": 7, "malice" : 6, "maison":"Griffondor"}
luna = { "nom": "Luna", "courage": 2, "malice" : 2, "maison":"Serdaigle"}
marcus = { "nom": "Marcus", "courage": 6, "malice" : 10, "maison":"Serpentar"}
lavande = { "nom": "Lavande", "courage": 10, "malice" : 6, "maison":"Griffondor"}
bole = { "nom": "Bole", "courage": 7, "malice" : 9, "maison":"Serpentar"}


a = { "nom": "a", "courage": 3, "malice" : 6, "maison":"Poufsouffle"}
b = { "nom": "b", "courage": 10, "malice" : 6, "maison":"Poufsouffle"}
c = { "nom": "c", "courage": 10, "malice" : 6, "maison":"Poufsouffle"}
d = { "nom": "d", "courage": 10, "malice" : 6, "maison":"Poufsouffle"}

promo1 = [adrian, hermione, luna, marcus, lavande, bole]

promo2 = [adrian, hermione, luna, marcus, lavande, bole, a, b, c, d]


entrees_visibles = [ (promo1, "Griffondor"), (promo1, "Poufsouffle") ]
entrees_invisibles = [ (promo2, "Griffondor"), (promo2, "Poufsouffle") ]


@solution
def nombre_eleves(promotion, maison):
    s = 0
    for eleve in promotion:
        if eleve["maison"] == maison:
            s+= 1
    return s

#  for p,m in entrees_visibles+entrees_invisibles:
    #  print(nombre_eleves(p,m))


