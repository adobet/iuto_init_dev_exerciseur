afrique = [ ('Silki', 4420), ('Kilimandjaro', 5892), ('Mont Cameroun', 4040)]
asie = [('Trivor', 7577), ('Annapurna I', 8091), ('Everest', 8848), ('K2', 8611)]

a=[('a', 1), ('b', 3), ('c', 6000), ('d', 2), ('e', 12), ('f', -18)]
r=[('a',0), ('b',2), ('c',32), ('d',8), ('e',16), ('f',4)]


entrees_visibles = [ afrique, asie ]
entrees_invisibles = [a, r]


@solution
def contient_sommet_exceptionnel(liste):
    for _,altitude in liste:
        if altitude >= 6000:
            return True
    return False


#  for e in entrees_visibles+entrees_invisibles:
    #  print(contient_sommet_exceptionnel(e))


