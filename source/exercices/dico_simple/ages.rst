Dictionnaire et âges
---------------------------------


On dispose de dictionnaires modélisant des groupes de personnes (les clefs sont des noms de personnes et les valeurs sont l'âge de ces personnes). Par exemple :

.. code-block:: python

   groupeA = {"Albert":17, "Bernard":14, "Chloé":17, "David":17, "Isidore":13}
   groupeB = {"Farid":19, "Gérard":16, "Hernestine":18, "Justine":18}



**Question 1.** Écrire une fonction ``moyenne_age`` qui prend un groupe en paramètre et qui renvoie la moyenne d'âge des personnes de ce groupe.


.. easypython:: ./moyenne_age/
   :language: Jacadi
   :titre: moyenne_age
   :extra_yaml:
     fichier_ens: moyenne_age.py


**Question 2.** crire une fonction ``contient_adulte`` qui prend un groupe en paramètre et qui vérifie si le groupe possède au moins un adulte (une personne qui a 18 ans ou plus).


.. easypython:: ./contient_age/
   :language: Jacadi
   :titre: contient_age
   :extra_yaml:
     fichier_ens: contient_age.py


**Question 3.** Écrire une fonction ``min_age`` qui prend un groupe en paramètre et qui renvoie le nom de la personne la plus jeune du groupe.


.. easypython:: ./min_age/
   :language: Jacadi
   :titre: min_age
   :extra_yaml:
     fichier_ens: min_age.py


**Question 4.** **Dictionnaire des fréquences**

Écrire une fonction ``resencement`` qui prend un groupe en paramètre et qui renvoie un dictionnaire dont les clefs sont les âges et les valeurs le nombre de personne de cet âge.


.. easypython:: ./dico_freq_age/
   :language: Jacadi
   :titre: dico_freq_age
   :extra_yaml:
     fichier_ens: dico_freq_age.py


