groupeA = {"Albert":17, "Bernard":14, "Chloé":17, "David":17, "Isidore":13}
groupeB = {"Farid":19, "Gérard":16, "Hernestine":18, "Justine":18}

a={'a': 1, 'b': 1, 'c': 1, 'd': 1, 'e': 1, 'f': 0}
r={'a':0, 'b':2, 'c':32, 'd':0, 'e':2, 'f':32}


entrees_visibles = [ groupeA, groupeB ]
entrees_invisibles = [a, r]


@solution
def resensement(groupe):
    res = {}
    for age in groupe.values():
        res[age] = res.get(age,0) + 1
    return res

#  for e in entrees_visibles+entrees_invisibles:
    #  print(recensement(e))


