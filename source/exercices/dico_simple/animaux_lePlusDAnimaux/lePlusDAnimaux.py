animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique', 'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}

a={'a': 'A', 'b': 'B', 'c': 'A', 'd': 'D', 'e': 'A', 'f': 'D'}

entrees_visibles = [ animaux ]           

entrees_invisibles = [ a ]

def nombreAnimauxParContinent(animaux) :
    d={}
    for continent in animaux.values():
        d[continent]=d.get(continent,0)+1
    return d
    
@solution
def lePlusdAnimaux (animaux) :
    maxi=None
    for (continent, nombre) in nombreAnimauxParContinent(animaux).items():
        if maxi==None or maxi<nombre:
            maxi=nombre
            lePlus=continent
    return lePlus

# print(lePlusdAnimaux (exemple))
            



