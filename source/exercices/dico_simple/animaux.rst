Dictionnaire et animaux
---------------------------------


.. image:: /images/iguane.png 
   :align: left
   :height: 4em




Echauffement
++++++++++++++

On dispose d'un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux.
Par exemple :

.. code-block:: python

   animaux={
       'éléphant': 'Asie', 'écureuil': 'Europe', 
       'panda': 'Asie', 'hippopotame': 'Afrique', 
       'girafe': 'Afrique', 'iguane': 'Amérique', 
       'lion': 'Afrique'}


Ecrire une fonction ``est_present`` qui indique si un continent apparait dans le dictionnaire.

.. easypython:: ./animaux_echauffement/
   :language: Jacadi
   :titre: Animaux echauffement
   :tags:
     - animaux
     - dico
     - verif
   :extra_yaml:
     fichier_ens: echauffement.py


.. image:: /images/ecureuil.jpeg
   :align: left
   :height: 4em


Le plus d'animaux
+++++++++++++++++++++

On dispose d'un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux.
Par exemple :

.. code-block:: python

   animaux={
       'éléphant': 'Asie', 'écureuil': 'Europe', 
       'panda': 'Asie', 'hippopotame': 'Afrique', 
       'girafe': 'Afrique', 'iguane': 'Amérique', 
       'lion': 'Afrique'}


Ecrire une fonction qui, à partir d'un tel dictionnaire,
détermine le continent ayant le plus grand nombre d'espèces recensées.

.. easypython:: ./animaux_lePlusDAnimaux/
   :language: Jacadi
   :titre: Animaux Le plus d'animaux
   :tags:
     - animaux
     - dico
     - max
   :extra_yaml:
     fichier_ens: lePlusDAnimaux.py


.. image:: /images/hippo.png
   :align: left
   :height: 4em


Recencement
+++++++++++++
On dispose d'un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux.
Par exemple :

.. code-block:: python

   animaux={
       'éléphant': 'Asie', 'écureuil': 'Europe', 
       'panda': 'Asie', 'hippopotame': 'Afrique', 
       'girafe': 'Afrique', 'iguane': 'Amérique', 
       'lion': 'Afrique'}


On veut écrire une fonction qui, à partir d'un tel dictionnaire,
retourne un dictionnaire avec le nombre d'espèces recensées par continent


.. easypython:: ./animaux_recencement/
   :language: Jacadi
   :titre: Animaux frequence
   :tags:
     - animaux
     - dico
     - freq   
   :extra_yaml:
     fichier_ens: recencement.py


.. image:: /images/elephant.png 
   :align: left
   :height: 4em

Continents avec le plus d'animaux
++++++++++++++++++++++++++++++++++

On dispose de deux dictionnaires :

* un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux.

  Par exemple :

.. code-block:: python

   animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 
       'panda': 'Asie', 'hippopotame': 'Afrique', 
       'girafe': 'Afrique', 'iguane': 'Amérique', 
       'lion': 'Afrique'}


* un autre dictionnaire indiquant, pour chaque animal, le nombre de représentants recencés.

  Par exemple :

.. code-block:: python

   representants={'éléphant': 250, 'écureuil': 2000000, 
       'mésange': 2580, 'panda': 500, 
       'hippopotame': 890, 'girafe': 2580,
       'iguane': 1450, 'lion': 1000}


Ecrire une fonction qui, à partir de ces deux dictionnaires,
retourne  le continent qui a le plus grand nombre d'animaux recencés (au total).

.. easypython:: ./animaux_continentLePlusDAnimaux/
   :language: Jacadi
   :titre: Animaux Continent le plus d'animaux
   :tags:
     - animaux
     - dico
     - joint
     - max   
   :extra_yaml:
     fichier_ens: continentLePlusDAnimaux.py


.. image:: /images/girafe.png
   :align: left
   :height: 4em



Continents
+++++++++++

On dispose de deux dictionnaires :

* un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux. Par exemple :

.. code-block:: python

   animaux={'éléphant': 'Asie', 'écureuil': 'Europe',
       'panda': 'Asie', 'hippopotame': 'Afrique',
       'girafe': 'Afrique', 'iguane': 'Amérique',
       'lion': 'Afrique'}``


* un autre dictionnaire indiquant, pour chaque animal, le nombre de représentants recensés. Par exemple :

.. code-block:: python
   
   representants={'éléphant': 250, 'écureuil': 2000000,
       'mésange': 2580, 'panda': 500,
       'hippopotame': 890, 'girafe': 2580,
       'iguane': 1450, 'lion': 1000}``



Ecrire une fonction qui, à partir de ces deux dictionnaires,
retourne pour chaque continent, les animaux qui y sont présents ainsi que le nombre de leurs représentants

.. easypython:: ./animaux_continents/
   :language: Jacadi
   :titre: Animaux Continents
   :tags:
     - animaux
     - dico
     - freq+
     - joint
   :extra_yaml:
     fichier_ens: continents.py



