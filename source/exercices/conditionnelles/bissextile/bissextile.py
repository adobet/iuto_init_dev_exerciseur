entrees_visibles = [2017,
                    2008,
                    2000,                   
]
entrees_invisibles = [
        1900,
        0,
        400,
        100,
]

@solution
def estBissextile (annee) :
    return ((annee%4==0 and annee%100!=0) or (annee%400==0))

