Plus de 8 caractères ?
------------------------


Écrire une fonction ``a_plus_de_8_caracteres()`` qui prend en paramètre une chaîne de caractères et qui indique si cette chaîne contient plus de 8 caractères.

.. TODO easypython:: a_plus_de_8_caracteres.py
   :language: python
   :uuid: 1231313

.. easypython:: ./a_plus_de_8_caracteres/
   :language: Jacadi
   :titre: a_plus_de_8_caracteres
   :extra_yaml:
     fichier_ens: a_plus_de_8_caracteres.py


