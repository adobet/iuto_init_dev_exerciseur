Somme
------------

Écrire une fonction ``somme`` qui prend en paramètre une liste de nombres et renvoie la somme de ces nombres.


.. easypython:: ./somme/
   :language: Jacadi
   :titre: somme
   :extra_yaml:
     fichier_ens: somme.py
