Moyenne
------------

Écrire une fonction ``moyenne`` qui prend en paramètre une liste de nombres et renvoie la moyenne de ces nombres.


.. easypython:: ./moyenne2/
   :language: Jacadi
   :titre: moyenne2
   :extra_yaml:
     fichier_ens: moyenne2.py
