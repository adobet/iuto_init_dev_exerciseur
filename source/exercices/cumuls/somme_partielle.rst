Somme partielle
-----------------

Écrire une fonction ``somme_partielle(liste, n)`` qui prend en paramètre une liste de nombres et un nombre, et renvoie la somme des n premiers nombres de la liste.


.. easypython:: ./somme_partielle/
   :language: Jacadi
   :titre: somme_partielle
   :extra_yaml:
     fichier_ens: somme_partielle.py
