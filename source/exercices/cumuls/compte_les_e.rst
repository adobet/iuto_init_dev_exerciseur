Combien de 'e' ?
---------------------------------

Écrire une fonction ``compte_les_e()`` qui prend en paramètre une chaîne de caractères et qui renvoie le nombre de ``e`` ou ``E`` que contient cette chaîne.

.. easypython:: ./compte_e/
   :language: Jacadi
   :titre: compte_e
   :extra_yaml:
     fichier_ens: compte_e.py
