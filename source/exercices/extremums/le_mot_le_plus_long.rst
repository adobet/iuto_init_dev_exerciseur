Le mot le plus long
--------------------

Ecrire une fonction renvoie le mot le plus long d'une liste de mots.
Attention, si plusieurs mots peuvent prétendre à cette particularité, la fonction devra renvoyer le premier de la liste.


.. easypython:: ./le_mot_le_plus_long/
   :language: Jacadi
   :titre: le_mot_le_plus_long
   :extra_yaml:
     fichier_ens: le_mot_le_plus_long.py
