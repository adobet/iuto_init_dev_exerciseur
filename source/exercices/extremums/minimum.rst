Minimum
-------

Ecrire une fonction renvoie le plus petit nombre d'une liste de nombres.
Si ce minimum n'existe pas, cette fonction devra renvoyer ``None``


.. easypython:: ./minimum/
   :language: Jacadi
   :titre: minimum
   :extra_yaml:
     fichier_ens: minimum.py
