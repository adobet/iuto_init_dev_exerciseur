entrees_visibles = [('a','programmation'),
                    ('z','programmation'),                 
]
entrees_invisibles = [
                    ('a',''),
                    ('a','a'),                 
                    ('p','programmation'),
                    ('n','programmation'),
]

@solution
def appartient (lettre, mot) :
  return lettre in mot
